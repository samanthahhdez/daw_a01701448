   IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO

            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO

EXECUTE creaMaterial 5000,'Martillos Acme',250,15

   SELECT * FROM MATERIALES

  <-- modificaMaterial que permite modificar un material que reciba como parámetros las columnas de la tabla materiales
  -- y actualice las columnas correspondientes con los valores recibidos, para el registro cuya llave sea la clave que se recibe como parámetro.-->

    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificaMaterial' AND type = 'P')
                DROP PROCEDURE modificaMaterial
            GO

            CREATE PROCEDURE modificaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                UPDATE Materiales SET Descripcion=@udescripcion,
                                        Costo=@ucosto,
                                        PorcentajeImpuesto=@uimpuesto
                                        WHERE Clave=@uclave
            GO

EXECUTE modificaMaterial 5000,'Martillos No Acme',250,15

SELECT * FROM Materiales


   <-- eliminaMaterial que elimina el registro de la tabla materiales cuya llave sea la clave que se recibe como parámetro.-->


    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'eliminaMaterial' AND type = 'P')
                DROP PROCEDURE eliminaMaterial
            GO

            CREATE PROCEDURE eliminaMaterial
                @uclave NUMERIC(5,0)
            AS
                DELETE FROM Materiales WHERE  Clave=@uclave
            GO

EXECUTE eliminaMaterial 5000

SELECT * FROM Materiales

   <----------------------------------------------------------------------PROYECTOS------------------------------------------------------------------>

   SELECT * FROM Proyectos
    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creaProyecto' AND type = 'P')
                DROP PROCEDURE creaProyecto
            GO

            CREATE PROCEDURE creaProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
                INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
            GO

EXECUTE creaProyecto 5020,'Depa limpio'

   SELECT * FROM Proyectos

  <-- modificaMaterial que permite modificar un material que reciba como parámetros las columnas de la tabla materiales
  -- y actualice las columnas correspondientes con los valores recibidos, para el registro cuya llave sea la clave que se recibe como parámetro.-->

    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificaProyecto' AND type = 'P')
                DROP PROCEDURE modificaProyecto
            GO

            CREATE PROCEDURE modificaProyecto
                 @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
                UPDATE Proyectos SET Denominacion=@udenominacion
                                        WHERE Numero=@unumero
            GO

EXECUTE modificaProyecto 5020,'Depa super limpio'

SELECT * FROM Proyectos


   <-- eliminaMaterial que elimina el registro de la tabla materiales cuya llave sea la clave que se recibe como parámetro.-->


    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'eliminaProyecto' AND type = 'P')
                DROP PROCEDURE eliminaProyecto
            GO

            CREATE PROCEDURE eliminaProyecto
                @unumero NUMERIC(5,0)
            AS
                DELETE FROM Proyectos WHERE Numero=@unumero
            GO

EXECUTE eliminaProyecto 5020

SELECT * FROM Proyectos



   <----------------------------------------------------------------------PROVEEDORES------------------------------------------------------------------>

   SELECT * FROM Proveedores
    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creaProveedor' AND type = 'P')
                DROP PROCEDURE creaProveedor
            GO

            CREATE PROCEDURE creaProveedor
                @urfc VARCHAR(15),
              @urazonsocial VARCHAR(20)
            AS
                INSERT INTO Proveedores VALUES(@urfc, @urazonsocial)
            GO

EXECUTE creaProveedor 9,'Paquito'

   SELECT * FROM Proveedores

  <-- modificaMaterial que permite modificar un material que reciba como parámetros las columnas de la tabla materiales
  -- y actualice las columnas correspondientes con los valores recibidos, para el registro cuya llave sea la clave que se recibe como parámetro.-->

    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificaProveedor' AND type = 'P')
                DROP PROCEDURE modificaProveedor
            GO

            CREATE PROCEDURE modificaProveedor
                 @urfc VARCHAR(20),
                @urazonsocial VARCHAR(50)
            AS
                UPDATE Proveedores SET RazonSocial=@urazonsocial
                                        WHERE RFC=@urfc
            GO

EXECUTE modificaProveedor '9','Juanin'

SELECT * FROM Proveedores


   <-- eliminaMaterial que elimina el registro de la tabla materiales cuya llave sea la clave que se recibe como parámetro.-->


    IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'eliminaProveedor' AND type = 'P')
                DROP PROCEDURE eliminaProveedor
            GO

            CREATE PROCEDURE eliminaProveedor
                @urfc VARCHAR(20)
            AS
                DELETE FROM Proveedores WHERE RFC=@urfc
            GO

EXECUTE eliminaProveedor '9'

SELECT * FROM Proveedores



   <----------------------------------------------------------------------ENTREGAN------------------------------------------------------------------>

 SET DATEFORMAT DMY
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creaEntregan' AND type = 'P')
                DROP PROCEDURE creaEntregan
            GO

            CREATE PROCEDURE creaEntregan
                @uclave NUMERIC(5,0),
				@uRFC VARCHAR(50),
				@uNumero NUMERIC(5,0),
				@uFecha DATETIME,
				@uCantidad DECIMAL(5,2)
            AS
                INSERT INTO Entregan VALUES(@uclave, @uRFC, @uNumero, @uFecha, @uCantidad)
            GO

EXECUTE creaEntregan 1400,'HHHH800101',5010,'24-12-2017', 666.00

SELECT *
FROM Entregan

IF EXISTS(SELECT name FROM sysobjects
          WHERE name = 'modificaEntregan' AND type = 'P')
		  DROP PROCEDURE modificaEntegan
	GO

		CREATE PROCEDURE modificaEntregan
			@uclave NUMERIC(5,0),
			@uRFC VARCHAR(50),
			@uNumero NUMERIC(5,0),
			@uFecha DATETIME,
			@uCantidad DECIMAL(5,2)
		AS
			UPDATE Entregan SET Fecha = @uFecha,
								Cantidad = @uCantidad
							WHERE Clave = @uclave AND RFC = @uRFC AND Numero = @uNumero
	GO

EXECUTE modificaEntregan 1400,'HHHH800101',5010,'09-01-1998', 420.00

SELECT *
FROM Entregan

IF EXISTS(SELECT name FROM sysobjects
          WHERE name = 'eliminaEntregan' AND type = 'P')
		  DROP PROCEDURE eliminaEntregan
	GO

		CREATE PROCEDURE eliminaEntregan
			@uclave NUMERIC(5,0),
			@uRFC VARCHAR(50),
			@uNumero NUMERIC(5,0),
			@uFecha DATETIME,
			@uCantidad DECIMAL(5,2)
		AS
			DELETE FROM Entregan WHERE Clave = @uclave
									AND RFC = @uRFC
									AND Numero = @uNumero
									AND Fecha = @uFecha
									AND Cantidad = @uCantidad
	GO

EXECUTE eliminaEntregan 1400,'HHHH800101',5010,'09-01-1998', 420.00

SELECT *
FROM Entregan