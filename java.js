
$('.ui.form')
  .form({
    on: 'blur',
    fields: {
      usuario: {
        identifier  : 'usuario',
        rules: [
          {
            type   : 'empty',
        prompt: "Introduce nombre de usuario."
          }
        ]
      },   
        nombre: {
        identifier  : 'nombre',
        rules: [
          {
            type   : 'empty',
               prompt: "Introduce tu nombre."
 
          }
        ]
      },
          apellido1: {
        identifier  : 'apellido1',
        rules: [
          {
            type   : 'empty',
           prompt: "Introduce tu apellido."
          }
        ]
      },
         match: {
        identifier  : 'contra1',
        rules: [
          {
            type   : 'match[contra]',
          prompt: "Las contraseñas no son iguales."
          },
             {
            type   : 'empty',
       prompt: "Introduce contraseña."
          }
        ]
      },
      correo: {
        identifier  : 'correo',
       rules: [
          {
            type: 'empty',
                 prompt: "Introduce tu correo."
           
          },
        ]
      },

    }
  })

function mostrar (){

$('.ui.modal')
  .modal('show')
;}

document.querySelector('#listo').addEventListener("click", function(btn) {
  mostrar();

});


function mostrar() {
  $('.ui.modal')
    .modal({
      blurring: true
    })
    .modal('show');
}

var buttons = document.querySelectorAll('#listo');
for (var i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener("click",function(btn){
    mostrar();

  });
}

