<?php
function conectar() {
        $servername ="localhost";
        $username=  "root";
        $password="";
        $dbname="comida";
        
        $con=mysqli_connect($servername, $username, $password, "comida");
        
        if(!$con){
            die("Connection failed: " . mysqli_connect_error());
        }
        return $con;
    
}
     
function desconectar($mysql) {
    mysqli_close($mysql);
}

function getRegistro($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM comida WHERE id="'.$registroId.'"';
     // Query execution; returns identifier of the result group
    $comidas = $db->query($query);   
    $fila = mysqli_fetch_array($comidas, MYSQLI_BOTH);
    return $fila;
}

function getComida(){
    $db=conectar();
    
$query = 'SELECT * FROM comida';
    $comidas=$db->query($query);
    $table='<table class="ui inverted teal table">
        <thead>
        <tr>
        <th>Nombre Estado</th>
        <th>Descripcion</th>
        <th>PIB</th>
        <th>Acciones  </th>
        
        <button class="ui icon circular medium right floated button" id="add" onclick="location=\'nuevo.php\'" >
  <i class="icon add"></i>
  
</button>

        </tr>
        </thead>
        <tbody>';
    while ($fila=mysqli_fetch_array($comidas,MYSQLI_BOTH)){
        $table.='
        <tr>
        <td>'.$fila["nombre"].'</td>
        <td>'.$fila["descripcion"].'</td>
        <td>'.$fila["precio"].'</td>
       <td><a href="edit.php?id='.$fila["id"].'">Edit</a>            -    <a href="delete.php?id='.$fila["id"].'">Delete</a></td>
         
        </tr>';
        
    }
        mysqli_free_result($comidas);
        desconectar($db);
        $table.="</tbody></table>";
    
        return $table;
        
    }
function guardarRegistro($nombre, $descripcion, $precio){
    $db = conectar();
    
    // insert command specification 
    $query='INSERT INTO comida (`nombre`, `descripcion` ,`precio`) VALUES (?,?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $nombre, $descripcion, $precio)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    desconectar($db);
}

function editarRegistro($id, $nombre, $descripcion, $precio){
    $db = conectar();
    
    // insert command specification 
    $query='UPDATE comida SET nombre=?, descripcion=?, precio=? WHERE id=?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssss", $nombre, $descripcion, $precio, $id)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
 

    
    desconectar($db);
}

function deleteInput($id){
      $db = conectar();
      // insert command specification
      $query='DELETE FROM comida WHERE id=?';
      // Preparing the statement
      if (!($statement = $db->prepare($query))) {
          die("Preparation failed: (" . $db->errno . ") " . $db->error);
      }
      // Binding statement params
      if (!$statement->bind_param("s", $id)) {
          die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
      }
      // update execution
      if ($statement->execute()) {
          echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
      } else {
          die("Update failed: (" . $statement->errno . ") " . $statement->error);
      }
       
    desconectar($db);
    }

/*
DELIMITER $ CREATE PROCEDURE comidanueva(IN uNombre VARCHAR(30), uDescripcion VARCHAR(140), uPrecio DECIMAL(6,2))
BEGIN
	  INSERT INTO comida (nombre, descripcion, precio, estado) VALUES (uNombre, uDescripcion, uPrecio);
END $*/
    
    
    function juju($name, $units, $quantity){
    $db=conectar();
        //Specification of the SQL query
        $query="CALL comidanueva2('".$name."', '".$units."', ".$quantity.")"; // LLamada al procedimiento Lab 21
         // Query execution; returns identifier of the result group  
        $line = mysqli_query($db, $query);
         desconectar($db);
        return $line;
}

?>