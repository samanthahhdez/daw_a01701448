<-- La suma de las cantidades e importe total de todas las entregas realizadas durante el 97-->
SET DATEFORMAT DMY
SELECT SUM(e.cantidad) as 'Cantidad total', sum(e.cantidad*(m.costo+(m.PorcentajeImpuesto*m.Costo))) as 'Importe total'
FROM ENTREGAN e, Materiales m
WHERE e.clave=m.clave
AND e.Fecha BETWEEN '01-01-1997' AND '31-12-1997'
SELECT*FROM PROVEEDORES
<--  Para cada proveedor, obtener la razón social del proveedor, número de entregas e importe total de las entregas realizadas. -->
SELECT p.RFC, p.RazonSocial, COuNT(p.rfc) as 'SUMA', sum(e.cantidad*(m.costo+(m.PorcentajeImpuesto*m.Costo))) as 'Importe total'
FROM ENTREGAN e, MATERIALES m, PROVEEDORES p
WHERE e.clave= m.clave
AND p.RFC=e.RFC
GROUP BY p.RFC, p.RazonSocial
<--Por cada material obtener la clave y descripción del material, la cantidad total entregada, la mínima cantidad entregada,
-- la máxima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400. -->
SELECT m.clave, m.descripcion, SUM(e.cantidad) as 'Cantidad total', MIN(e.cantidad) as 'Minima', MAX(e.cantidad) as 'Maxima', sum(e.cantidad*(m.costo+(m.PorcentajeImpuesto*m.Costo))) as 'Importe total', AVG (e.cantidad) as 'Promedio'
FROM Materiales m, Entregan e
WHERE m.clave=e.clave
GROUP BY m.clave, m.descripcion
HAVING AVG (e.cantidad) > 400
<--Para cada proveedor, indicar su razón social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripción
-- del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500. -->
SELECT p.RazonSocial, AVG (E.cantidad), e.clave, m.descripcion
FROM MATERIALES m, PROVEEDORES p, ENTREGAN e
WHERE m.Clave=e.clave AND e.RFC=p.RFC
GROUP BY p.RazonSocial, e.clave, m.descripcion
HAVING AVG(e.cantidad)>500
<--Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos
-- grupos de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y
--  aquellos para los que la cantidad promedio entregada sea mayor a 450. -->
SELECT p.RazonSocial, AVG (E.cantidad), e.clave, m.descripcion
FROM MATERIALES m, PROVEEDORES p, ENTREGAN e
WHERE m.Clave=e.clave AND e.RFC=p.RFC
GROUP BY p.RazonSocial, e.clave, m.descripcion
HAVING AVG(e.cantidad)>450 OR AVG(e.cantidad)<370


INSERT INTO materiales VALUES (1480,'Metalin', 130, 3) ;

<-- Clave y descripción de los materiales que nunca han sido entregados. -->
SELECT M.Clave, M.Descripcion
FROM Materiales m
WHERE M.clave NOT IN (select m.clave
  from Entregan e, Materiales m
  WHERE e.clave=m.clave
)


SELECT p.RazonSocial
FROM Proveedores p, Entregan e, Proyectos pr
WHERE p.RFC = e.RFC AND e.Numero = pr.Numero
AND pr.Denominacion = 'Vamos mexico'
AND p.RazonSocial IN (SELECT p.RazonSocial
		FROM Proveedores p, Entregan e, Proyectos pr
		WHERE p.RFC = e.RFC AND e.Numero = pr.Numero
		AND pr.Denominacion = 'Queretaro limpio')
GROUP BY p.RazonSocial

--Descripción de los materiales que nunca han sido entregados al proyecto 'CIT Yucatán'.

SELECT m.Descripcion
FROM Materiales m
WHERE m.Clave NOT IN (SELECT m.Clave
						FROM Materiales m, Entregan e, Proyectos p
						WHERE m.Clave = e.Clave AND p.Numero = e.Numero
						AND p.Denominacion = 'CIT Yucatan')

--Razón social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada
--es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'.

SELECT p.RFC, AVG(e.Cantidad) as 'Cantidad'
FROM Proveedores p, Entregan e
WHERE p.RFC = e.RFC
GROUP BY p.RFC
HAVING AVG(e.Cantidad) > (SELECT AVG(e.Cantidad)
							FROM Proveedores p, Entregan e
							WHERE p.RFC = e.RFC AND e.RFC = 'VAGO780901'
							GROUP BY p.RFC)

-- RFC, razón social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades
--totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001.

CREATE VIEW ENTREGAS_DURANGO2001(RFC, CantidadTotal)
AS
		SELECT e.RFC, SUM(e.Cantidad)
		FROM ENTREGAS_DURANGO1 ed, Entregan e
		WHERE ed.RFC = e.RFC
		AND e.Fecha BETWEEN '01-01-2001' AND '31-12-2001'
		GROUP BY e.RFC

CREATE VIEW ENTREGAS_DURANGO2000(RFC, CantidadTotal)
AS
		SELECT e.RFC, SUM(e.Cantidad)
		FROM ENTREGAS_DURANGO1 ed, Entregan e
		WHERE ed.RFC = e.RFC
		AND e.Fecha BETWEEN '01-01-2000' AND '31-12-2000'
		GROUP BY e.RFC

SELECT p.RazonSocial, p.RFC
FROM ENTREGAS_DURANGO2000 ed2000, ENTREGAS_DURANGO2001 ed2001, Proveedores p
WHERE ed2000.RFC = ed2001.RFC AND
ed2000.RFC = p.RFC
AND ed2000.CantidadTotal > ed2001.CantidadTotal
Fin de la conversación de chat
Escribe un mensaje...





